async function getVerse() {
    const res = await fetch('verse')
    verse = await res.json()
    createTimeout()
}

function updateUI() {
    eleHeader.textContent = `${verse.book} ${verse.chapter ? `${verse.chapter}:` : ``}${verse.verse}`;
    eleMain.innerHTML = verse.text.replaceAll('\n', '<br>').replaceAll('  ', '&nbsp;&nbsp;')
}

async function update() {
    await getVerse()
    updateUI()
}

function removeTimeout() {
    clearTimeout(timeoutId)
}

function createTimeout() {
    removeTimeout()
    timeoutId = setTimeout(async () => await update(), new Date(verse.nextVerseTime).getTime() - Date.now())
}

let eleHeader
let eleMain
let timeoutId
/** @type {{book: string,  chapter: number,  verse: number,  text: string,  verseTime: number,  nextVerseTime: number}} */
let verse

self.addEventListener('load', async () => {
    eleHeader = document.querySelector('header')
    eleMain = document.querySelector('main')
    await update()
})

document.addEventListener('visibilitychange', async () => {
    if (document.hidden) {
        removeTimeout()
    } else {
        if (verse && new Date() > new Date(verse.nextVerseTime)) {
            await update()
        } else {
            createTimeout()
        }
    }
})
