import { bible } from './bible.ts'
import { ESV_TOKEN } from './token.ts'
import { assertEquals } from '$std/assert/mod.ts'

const BASE_API = 'https://api.esv.org/v3/passage/text/?include-passage-references=false&include-verse-numbers=false&include-first-verse-numbers=false&include-footnotes=false&include-footnote-body=false&include-headings=false&include-short-copyright=false'

/** Response from ESV API assuming a valid single verse query. */
interface ESVAPIResponse {
  /** e.g., Genesis 1:1 */
  query: string;
  /** Genesis 1:1 */
  canonical: string;
  /** e.g., [1001001, 1001001] */
  parsed: [[number, number]];
  passage_meta: [
    {
      /** e.g., Genesis 1:1 */
      canonical: string;
      /** e.g., [1001001, 1001031] */
      chapter_start: [number, number];
      /** e.g., [1001001, 1001031] */
      chapter_end: [number, number];
      prev_verse: number | null;
      /** e.g., 1001002 */
      next_verse: number;
      /** e.g., 1001001 */
      prev_chapter: number | null;
      /** e.g., [1002001, 1002025] */
      next_chapter: [number, number];
    },
  ];
  /** e.g., ["  In the ..... the earth.\n\n"] */
  passages: Array<string>;
}

export type GetSingleRandomeVerseResponse = {
  status: 'FAILURE'
  text: string
} | {
  status: 'SUCCESS'
  book: string
  chapter: number
  verse: number | null
  text: string
}

/** Array of all bible verses as used by ESV API query parameter.  ['001001001', ...] */
const expandedVerses: Array<string> = []

for (let book = 0; book < bible.length; ++book) {
  for (let chapter = 0; chapter < bible[book].length; ++chapter) {
    for (let verse = 1; verse <= bible[book][chapter]; ++verse)
      expandedVerses.push(`${String(book + 1).padStart(3, '0')}${String(chapter + 1).padStart(3, '0')}${verse.toString().padStart(3, '0')}`)
  }
}

Deno.test('first verse', () => assertEquals('001001001', expandedVerses.at(0)))
Deno.test('number of verses', () => assertEquals(31_103, expandedVerses.length))

export async function getSingleRandomVerse(): Promise<GetSingleRandomeVerseResponse> {
  const randomVerse = expandedVerses[Math.floor(expandedVerses.length * Math.random())]

  console.info(`getSingleRandomVerse - requesting: [${randomVerse}]`)

  const res = await fetch(`${BASE_API}&q=${randomVerse}`, { headers: { Authorization: `Token ${ESV_TOKEN}` }, })

  if (res.status !== 200) {
    const resText = await res.text()
    const errorMessage = `getSingleRandomVerse - status: [${res.status}] status text: [${res.statusText}] verse: [${randomVerse}] text: [${resText}]`
    console.error(errorMessage)
    return { status: 'FAILURE', text: errorMessage }
  } else {
    const apiRes: ESVAPIResponse = await res.json();

    if (apiRes.passages.length === 1) {
      const [, book, , chapter, verse] = [...(apiRes.canonical.matchAll(/(.*) (([0-9]+):)?([0-9]+)/g))][0]

      return {
        status: 'SUCCESS',
        book,
        chapter: +chapter,
        verse: +verse,
        text: apiRes.passages.at(0)!.trim()
      }
    } else {
      const errorText = `getSingleRandomVerse - The ESV API does not like [${apiRes.query}]`
      console.error(errorText)
      return { status: 'FAILURE', text: errorText }
    }
  }
}
