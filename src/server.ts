import { serveFile } from '$std/http/file_server.ts'
import { getSingleRandomVerse } from './esv-api.ts'

interface VerseAPIResponse {
  book: string,
  chapter: number,
  verse: number | null,
  text: string,
  verseTime: string,
  nextVerseTime: string
}

/** The delay (in milliseconds) before a new verse is retrieved from ESV API. */
const NEW_VERSE_DELAY = 30_000
const KV_VERSE = 'verse'
const KV_LOCK = 'lock'
const kv = await Deno.openKv();

Deno.serve(async (request) => {

  console.info(`server.js - requested: [${request.url}]`)

  if (request.url.endsWith('verse')) {
    let verse!: VerseAPIResponse
    let res = { ok: false }


    while (!res.ok) {
      const kvLock = await kv.get<boolean>([KV_LOCK])
      const kvVerse = await kv.get<VerseAPIResponse>([KV_VERSE])

      verse = kvVerse.value ?? {
        book: '',
        chapter: 0,
        verse: 0,
        text: '',
        verseTime: '',
        nextVerseTime: new Date(0).toISOString(),
      }

      if (new Date() > new Date(verse.nextVerseTime) && !kvLock.value) {

        res = await kv.atomic().check(kvVerse).check(kvLock).set([KV_LOCK], true).commit()

        if (res.ok) {
          const newVerse = await getSingleRandomVerse()

          if (newVerse.status === 'SUCCESS') {
            verse.book = newVerse.book
            verse.chapter = newVerse.chapter
            verse.verse = newVerse.verse
            verse.text = newVerse.text
            verse.verseTime = new Date().toISOString()
            verse.nextVerseTime = new Date(Date.now() + NEW_VERSE_DELAY).toISOString()

            console.info(`server.js - attempting to save verse to database [${verse.book} ${verse.chapter}:${verse.verse}]`)

            res = await kv.atomic().check(kvVerse).set([KV_VERSE], verse).commit()

            if (res.ok) {
              console.info('server.js - successfully saved')
            } else {
              console.error('server.js - unsuccessful save attempt')
            }
          } else {
            console.error(`server.ts - ${newVerse.text}`)
          }

          await kv.delete([KV_LOCK])
        }
      } else {
        res.ok = true
      }
    }

    return new Response(JSON.stringify(verse), { headers: { 'Content-Type': 'application/json' } })
  } else if (request.url.endsWith('main.js')) {
    return await serveFile(request, `${Deno.cwd()}/static/main.js`)
  } else if (request.url.endsWith('copyright.html')) {
    return await serveFile(request, `${Deno.cwd()}/static/copyright.html`)
  } else if (request.url.endsWith('main.css')) {
    return await serveFile(request, `${Deno.cwd()}/static/main.css`)
  } else {
    return await serveFile(request, `${Deno.cwd()}/static/index.html`)
  }
})
